package com.labs.digital.aval.training.semana1

import java.util.*


data class CorrectAndWrong(val rightPosition: Int, val wrongPosition: Int)

fun evaluate(secret: String, guess: String): CorrectAndWrong {
    var rightPosition = 0
    var wrongPosition = 0
    var indexGuess = 0
    var indexSecret=0
    var existChars=""

    for (letterSecret: Char in secret) {
        indexGuess=0
        for (letterGuess: Char in guess) {
            if(letterSecret == letterGuess && indexSecret == indexGuess){
                rightPosition += 1
                existChars=existChars.plus(letterSecret)
                break
            }else if(letterSecret == letterGuess && indexSecret != indexGuess && !existChars.contains(letterGuess)){
                wrongPosition += 1
                break
            }
            indexGuess ++
        }
        indexSecret ++

    }

    return CorrectAndWrong(rightPosition, wrongPosition)
}