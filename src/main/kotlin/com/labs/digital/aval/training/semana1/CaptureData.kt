package com.labs.digital.aval.training.semana1

import java.util.*

fun getInput(operation: String): String {
    var readerInput = ""
    var reader = Scanner(System.`in`)
    do {
        println("Ingrese 4 Letras que contengan A, B, C, D, E, F, $operation");
        readerInput = reader.nextLine()
    } while (!validateCharactersLength(readerInput) && !validateCharactersRan(readerInput))

    return readerInput
}

fun validateCharactersLength(inputSecret: String): Boolean {
    return inputSecret.length == 4
}

fun validateCharactersRan(inputSecret: String): Boolean {
    return inputSecret.asIterable().all { it in 'A'..'F' } || inputSecret.asIterable().all { it in 'a'..'f' }
}

fun main() {
    val secret = getInput("SECRET")
    val guess = getInput("GUESS")
    val actual = evaluate(secret, guess)

    println(actual)
}